FROM python:3.7-alpine

RUN apk add --update --no-cache \
    bash\
    groff\
    postgresql-client

RUN pip install awscli==1.16.98